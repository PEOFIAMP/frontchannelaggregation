#! /bin/bash

./configure --with-log4shib=/opt/shibboleth-sp \
	--enable-apache-22 \
	-with-apxs2=/usr/sbin/apxs \
	--prefix=/opt/shibboleth-sp \
	--with-xerces=/opt/shibboleth-sp \
	--with-xmlsec=/opt/shibboleth-sp \
	--with-xmltooling=/opt/shibboleth-sp \
	--with-saml=/opt/shibboleth-sp
